FROM node:12-alpine
ENV REPO_HOME=/app
WORKDIR ${REPO_HOME}

COPY . .

RUN npm install

RUN addgroup -S crojasp && \
    adduser -s /bin/bash -S -G crojasp crojasp
USER crojasp

CMD [ "node", "index.js"]
